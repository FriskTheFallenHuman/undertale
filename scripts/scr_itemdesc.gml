global.msg[0] = "* If you are reading this,&  I messed up somehow./%"
switch argument0
{
    case 0:
        global.msg[0] = "* If you are reading this,&  I messed up somehow./%"
        break
    case 1:
        global.msg[0] = "* " + '"' + "Monster Candy" + '"' + " - Heals 10 HP&* Has a distinct,^1 &  non-licorice flavor./%"
        break
    case 2:
        global.msg[0] = "* " + '"' + "Croquet Roll" + '"' + " - Heals 15 HP&* Fried dough traditionally&  served with a mallet./%"
        break
    case 3:
        global.msg[0] = "* " + '"' + "Stick" + '"' + " - Weapon AT 0&* Its bark is worse than&  its bite./%"
        break
    case 4:
        global.msg[0] = "* " + '"' + "Bandage" + '"' + " - Heals 10 HP&* It has already been used&  several times./%"
        break
    case 5:
        global.msg[0] = "* " + '"' + "Rock Candy" + '"' + " - Heals 1 HP&* Here is a recipe to make&  this at home:/"
        global.msg[1] = "* 1. Find a rock/%"
        break
    case 6:
        global.msg[0] = "* " + '"' + "Pumpkin Rings" + '"' + " - Heals 8 HP&* A small pumpkin cooked&  like onion rings./%"
        break
    case 7:
        global.msg[0] = "* " + '"' + "Spider Donut" + '"' + " - Heals 12 HP&* A donut made with Spider&  Cider in the batter./%"
        break
    case 8:
        global.msg[0] = "* " + '"' + "Stoic Onion" + '"' + " - Heals 5 HP&* Even eating it raw^1, the&  tears just won't come./%"
        break
    case 9:
        global.msg[0] = "* " + '"' + "Ghost Fruit" + '"' + " - Heals 16 HP&* If eaten^1, it will never&  pass to the other side./%"
        break
    case 10:
        global.msg[0] = "* " + '"' + "Spider Cider" + '"' + " - Heals 24 HP&* Made with whole spiders^1,&  not just the juice./%"
        break
    case 11:
        global.msg[0] = "* " + '"' + "Butterscotch Pie" + '"' + " - All HP&* Butterscotch-cinnamon&  pie^1, one slice./%"
        break
    case 12:
        global.msg[0] = "* " + '"' + "Faded Ribbon" + '"' + " - Armor DF 3&* If you're cuter^1, monsters&  won't hit you as hard./%"
        break
    case 13:
        global.msg[0] = "* " + '"' + "Toy Knife" + '"' + " - Weapon AT 3&* Made of plastic.&* A rarity nowadays./%"
        break
    case 14:
        global.msg[0] = "* " + '"' + "Tough Glove" + '"' + " - Weapon AT 5&* A worn pink leather glove^1.&* For five-fingered folk./%"
        break
    case 15:
        global.msg[0] = "* " + '"' + "Manly Bandanna" + '"' + " - Armor DF 7&* It has seen some wear.&* It has abs drawn on it./%"
        break
    case 16:
        global.msg[0] = "* " + '"' + "Snowman Piece" + '"' + " - Heals 45 HP&* Please take this to the&  ends of the earth./%"
        break
    case 17:
        global.msg[0] = "* " + '"' + "Nice Cream" + '"' + " - Heals 15 HP&* Instead of a joke,^1 the&  wrapper says something nice./%"
        break
    case 18:
        global.msg[0] = "* " + '"' + "Puppydough Icecream" + '"' + "&* Heals 28 HP^1.&* Made by young pups./%"
        break
    case 19:
        global.msg[0] = "* " + '"' + "Bisicle" + '"' + " - Heals 11 HP&* It's a two-pronged popsicle^1,&  so you can eat it twice./%"
        break
    case 20:
        global.msg[0] = "* " + '"' + "Unisicle" + '"' + " - Heals 11 HP&* It's a SINGLE-pronged popsicle^1.&* Wait^1, that's just normal.../%"
        break
    case 21:
        global.msg[0] = "* " + '"' + "Cinnamon Bunny" + '"' + " - Heals 22 HP&* A cinnamon roll in the shape&  of a bunny./%%"
        break
    case 22:
        global.msg[0] = "* " + '"' + "Temmie Flakes" + '"' + " - Heals 2 HP&* It's just torn up pieces&  of colored construction paper./%%"
        break
    case 23:
        global.msg[0] = "* " + '"' + "Abandoned Quiche" + '"' + " Heals 34 HP&* A psychologically damaged&  spinach egg pie./%%"
        break
    case 24:
        global.msg[0] = "* " + '"' + "Old Tutu" + '"' + " - Armor DF 10&* Finally^1, a protective piece&  of armor./%%"
        break
    case 25:
        global.msg[0] = "* " + '"' + "Ballet Shoes" + '"' + " - Wpn AT 7&* These used shoes make you&  feel incredibly dangerous./%%"
        break
    case 26:
        global.msg[0] = "* " + '"' + "Punch Card" + '"' + " - Battle Item&* Use to make punching attacks&  stronger in one battle./"
        global.msg[1] = "* Use outside of battle to&  look at the card./%%"
        break
    case 27:
        global.msg[0] = "* " + '"' + "Annoying Dog" + '"' + " - Dog&* A little white dog^1.&* It's fast asleep.../%%"
        break
    case 28:
        global.msg[0] = "* " + '"' + "Dog Salad" + '"' + " - Heals ?? HP&* Recovers HP.&* (Hit Poodles.)/%%"
        break
    case 29:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Shiny trail left&  behind by a dog./%%"
        break
    case 30:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Dog-shaped husk shed&  from a dog's carapace./%%"
        break
    case 31:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Dirty dishes left&  unwashed by a dog./%%"
        break
    case 32:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Glowing crystals secreted&  by a dog./%%"
        break
    case 33:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Jigsaw puzzle left&  unfinished by a dog./%%"
        break
    case 34:
        global.msg[0] = "* " + '"' + "Dog Residue" + '"' + " - Dog Item&* Web spun by a dog&  to ensnare prey./%%"
        break
    case 35:
        global.msg[0] = "* " + '"' + "Astronaut Food" + '"' + " Heals 21 HP&* For feeding a pet&  astronaut./%%"
        break
    case 36:
        global.msg[0] = "* " + '"' + "Instant Noodles" + '"' + " Heals HP&* Comes with everything you&  need for a quick meal!/%%"
        break
    case 37:
        global.msg[0] = "* " + '"' + "Crab Apple" + '"' + " Heals 18 HP&* An aquatic fruit that&  resembles a crustacean./%%"
        break
    case 38:
        global.msg[0] = "* " + '"' + "Hot Dog...?" + '"' + " Heals 20 HP&* The " + '"' + "meat" + '"' + " is made of something&  called a " + '"' + "water sausage." + '"' + "/%%"
        break
    case 39:
        global.msg[0] = "* " + '"' + "Hot Cat" + '"' + " Heals 21 HP&* Like a hot dog^1, but with&  little cat ears on the end./%%"
        break
    case 40:
        global.msg[0] = "* " + '"' + "Glamburger" + '"' + " Heals 27 HP&* A hamburger made of edible&  glitter and sequins./%%"
        break
    case 41:
        global.msg[0] = "* " + '"' + "Sea Tea" + '"' + " Heals 10 HP&* Made from glowing marshwater.&* Increases SPEED for one battle./%%"
        break
    case 42:
        global.msg[0] = "* " + '"' + "Starfait" + '"' + " Heals 14 HP&* A sweet treat made of&  sparkling stars./%%"
        break
    case 43:
        global.msg[0] = "* " + '"' + "Legendary Hero" + '"' + " Heals 40 HP&* Sandwich shaped like a sword.&* Increases ATTACK when eaten./%%"
        break
    case 44:
        global.msg[0] = "* " + '"' + "Cloudy Glasses" + '"' + " - Armor DF 6&* Glasses marred with wear.&* Increases INV by 9./"
        global.msg[1] = "* (After you get hurt by an&  attack^1, you stay invulnerable&  for longer.)/%%"
        break
    case 45:
        global.msg[0] = "* " + '"' + "Torn Notebook" + '"' + " - Weapon AT 2&* Contains illegible scrawls.&* Increases INV by 6./%"
        global.msg[1] = "* (After you get hurt by an&  attack^1, you stay invulnerable&  for longer.)/%%"
        break
    case 46:
        global.msg[0] = "* " + '"' + "Stained Apron" + '"' + " - Armor DF 11&* Heals 1 HP every other&  turn./%"
        break
    case 47:
        global.msg[0] = "* " + '"' + "Burnt Pan" + '"' + " - Weapon AT 10&* Damage is rather consistent.&* Consumable items heal 4 more HP./%"
        break
    case 48:
        global.msg[0] = "* " + '"' + "Cowboy Hat" + '"' + " - Armor DF 12&* This battle-worn hat makes you&  want to grow a beard./"
        global.msg[1] = "* It also raises ATTACK by 5./%"
        break
    case 49:
        global.msg[0] = "* " + '"' + "Empty Gun" + '"' + " - Weapon AT 12&* An antique revolver^1.&* It has no ammo./"
        global.msg[1] = "* Must be used precisely, or&  damage will be low./%"
        break
    case 50:
        global.msg[0] = "* " + '"' + "Heart Locket" + '"' + " - Armor DF 15&* It says " + '"' + "Best Friends Forever." + '"' + "/%"
        break
    case 51:
        global.msg[0] = "* " + '"' + "Worn Dagger" + '"' + " - Weapon AT 15&* Perfect for cutting plants&  and vines./%"
        break
    case 52:
        global.msg[0] = "* " + '"' + "Real Knife" + '"' + " - Weapon AT 99&* Here we are!/%"
        break
    case 53:
        global.msg[0] = "* " + '"' + "The Locket" + '"' + " - Armor DF 99&* You can feel it beating./%"
        break
    case 54:
        global.msg[0] = "* " + '"' + "Bad Memory" + '"' + " Hurts 1 HP&* ?????/%%"
        break
    case 55:
        global.msg[0] = "* " + '"' + "Dream" + '"' + " Heals 12 HP&* The goal of " + '"' + "Determination." + '"' + "/%%"
        break
    case 56:
        global.msg[0] = "* " + '"' + "Undyne's Letter" + '"' + " - Unique&* Letter written for Dr.&  Alphys./%%"
        break
    case 57:
        global.msg[0] = "* " + '"' + "Undyne's Letter EX" + '"' + " - Unique&* It has DON'T DROP IT&  written on it./%%"
        break
    case 58:
        global.msg[0] = "* " + '"' + "Popato Chisps" + '"' + " Heals 13 HP&* Regular old popato chisps./%%"
        break
    case 59:
        global.msg[0] = "* " + '"' + "Junk Food" + '"' + " Heals 17 HP&* Food that was probably&  once thrown away./%%"
        break
    case 60:
        global.msg[0] = "* " + '"' + "Mystery Key" + '"' + " Unique&* It is too bent to fit on&  your keychain./%%"
        break
    case 61:
        global.msg[0] = "* " + '"' + "Face Steak" + '"' + " Heals 60 HP&* Huge steak in the shape&  of Mettaton's face./"
        global.msg[1] = "* (You don't feel like it's&  made of real meat...)/%%"
        break
    case 62:
        global.msg[0] = "* " + '"' + "Hush Puppy" + '"' + " Heals 65 HP&* This wonderful spell will stop&  a dog from casting magic./%%"
        break
    case 63:
        global.msg[0] = "* " + '"' + "Snail Pie" + '"' + " - Heals Some HP&* An acquired taste./%%"
        break
    case 64:
        global.msg[0] = "* " + '"' + "temy armor" + '"' + " - Armor DF 20&* The things you can do with&  a college education!/"
        global.msg[1] = "* Raises ATTACK when worn^1.&* Recovers HP every other turn^1.&* INV up slightly./%%"
        break
}
